<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Api extends Model
{
    public static function saveRecord($params = false) {
        if($params) {
            $id = DB::table($params["table"])->insertGetId($params["data"]);
            return $id;
        }
        return false;
    }

    public static function getRegionItems() {
        $record = DB::table('region_items')
            ->select(
                'region_items.*',
                'regions.name'
            )
            ->join('regions', 'regions.id', '=', 'region_items.region_id')
            ->orderBy('region_items.id', 'asc')
            ->get();

        return $record;
    }

    public static function getStationItems($id = "S109") {
        $record = DB::table('stations')
            ->where('stations.id', $id)
            ->first();

        return $record;
    }

    public static function truncateTables() {
        DB::table('regions')->truncate();
        DB::table('region_items')->truncate();
        DB::table('stations')->truncate();
    }
}