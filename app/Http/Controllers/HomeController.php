<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class HomeController extends Controller
{
    public function index() {
        $data               = array();
        $data["pageTitle"]  = 'Pollution Standard Index (PSI)';

        /*
         * normal way of loading data and dispalyingn to view
         */
        // $data["regionItems"]        = Api::getRegionItems();
        // $stationItems               = Api::getStationItems("S109");
        // $stationItems->timestamp    = ($stationItems) ? date("Y m d H:i:s", strtotime($stationItems->timestamp)) : "";
        // $data["stationItems"]       = $stationItems;

        return view('home', $data);
    }
}
