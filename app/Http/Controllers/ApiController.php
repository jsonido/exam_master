<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class ApiController extends Controller
{
    public function index() {

        //truncate all table
        Api::truncateTables();

        //call external api via curl
        $regions    = json_decode($this->curlData("regions"));
        $stations   = json_decode($this->curlData("stations"));
        
        //make sure data has value
        if($regions) {
            
            //iterate through record
            foreach($regions->region_metadata as $region) {

                //collect record
                $record         = array();
                $record["name"] = $region->name;
                $record["lat"]  = ($region->label_location) ? $region->label_location->latitude  : "0";
                $record["lon"]  = ($region->label_location) ? $region->label_location->longitude : "0";
                
                //prepare data to save
                $params             = array();
                $params["table"]    = "regions";
                $params["data"]     = $record;

                //save record to db
                $id = Api::saveRecord($params);

                //make sure successfully saved
                if($id) {

                    //target data
                    $items = $regions->items[0];
                    
                    //make sure has value
                    if( isset($items->readings) ) {
                        
                        //collect record
                        $_record                        = array();
                        $_record["region_id"]           = $id;
                        $_record["timestamp"]           = date("Y-m-d H:i:s", strtotime($items->timestamp));
                        $_record["update_timestamp"]    = date("Y-m-d H:i:s", strtotime($items->update_timestamp));
                        
                        //iterate through record
                        foreach($items->readings as $key => $reading) {
                            
                            //make sure has value
                            if($reading) {

                                //iterate through record
                                foreach($reading as $index => $value) {

                                    //make sure target matches
                                    if($region->name == $index) {
                                        $_record[$key] = $value;
                                    }
                                }
                            }
                            else {
                                $_record[$key] = "";
                            }

                        }

                        //prepare data to save
                        $params             = array();
                        $params["table"]    = "region_items";
                        $params["data"]     = $_record;

                        //save record to db
                        Api::saveRecord($params);
                    }

                }
            }
        }

        //make sure data has value
        if($stations) {

            //iterate through record
            foreach($stations->metadata->stations as $station) {

                //collect record
                $record                 = array();
                $record["id"]           = $station->id;
                $record["device_id"]    = $station->device_id;
                $record["name"]         = $station->name;
                $record["lat"]          = ($station->location) ? $station->location->latitude  : "0";
                $record["lon"]          = ($station->location) ? $station->location->longitude : "0";
                
                //target data
                $items = $stations->items[0];

                //make sure has value
                if( isset($items->readings) ) {

                    $record["timestamp"] = date("Y-m-d H:i:s", strtotime($items->timestamp));
                
                    //iterate through record
                    foreach($items->readings as $rec) {

                        //make sure has matching value, then break
                        if($rec->station_id == $station->id) {
                            $record["value"] = $rec->value;
                            break;
                        }
                    }
                
                }
                
                //prepare data to save
                $params             = array();
                $params["table"]    = "stations";
                $params["data"]     = $record;

                //save record to db
                $id = Api::saveRecord($params);
            }
        }

        return redirect()->action('HomeController@index');
    }

    public function curlData($type = "regions") {
        $url = ($type == "regions") ? "https://api.data.gov.sg/v1/environment/psi" : "https://api.data.gov.sg/v1/environment/air-temperature";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "GET",
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function getPsi() {
        $data["psi"] = $this->getRegionItems();
        $data["air"] = $this->getStationItems("S109");

        return response()->json($data, 200);
    }

    private function getRegionItems() {
        $regionItems = Api::getRegionItems();
        return $regionItems;
    }

    private function getStationItems($id = "S109") {
        $stationItems = Api::getStationItems($id);

        if($stationItems) {
            $stationItems->timestamp = date("Y m d H:i:s", strtotime($stationItems->timestamp));
        }
        
        return $stationItems;
    }
}
