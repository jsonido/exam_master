<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
<meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>SPH : PHP Assignment</title>
    <link rel="icon" type="image/png" sizes="32x32" href="images/common/favicon.png">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">

    <script>
        var base_url = "<?php echo e(URL::to('/')); ?>";
        console.log("base_url: "+base_url);
    </script>
</head>
<body>
    <!-- if lt IE 10p.browserupgrade You are using an strong outdated browser. Please a(href='http://browsehappy.com/') upgrade your browser to improve your experience.
    -->
    
    <div id="overlayer">
        <div class="ball-wrap">
            <div class="bounceball"></div>
        </div>
    </div>

    <div class="wrap fluid-container">
        <main class="main">
            <div class="page-content">
                <div class="section-container"> 
                    <!-- Page Header - START-->
                    <div class="comp comp-page-short-header inverse" id="ch-1">
                        <div class="comp-holder">
                            <div class="hero-image-bg" style="background-color:#1c1c45">
                                <div class="container">
                                    <div class="hero-header-text">
                                        <h1>Pollutant Standards Index (PSI)</h1>
                                        <p class="subtext"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page Header - END-->
                </div>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </main>
    </div>

    <footer class="fluid-container"> 
      <div class="container">
        <div class="row">
          <div class="col-9 col newspaper-links">
            <div class="copy"><span>Copyright © 2020 Singapore Press Holdings Ltd. Co. Regn. No. 198402868E. All Rights Reserved. <br></span>1000 Toa Payoh North Annexe Level 6, News Centre Singapore 318994</div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <script src="<?php echo e(asset('js/api.js')); ?>" defer></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\laravel\exam-master\resources\views/layouts/app.blade.php ENDPATH**/ ?>