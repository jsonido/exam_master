<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="section-container account">  
                <div class="container account-container">  
                    <div class="row">

                        <div class="col-md-9 account-left">
                            <p class="option-label">Keep track of PSI</p>
                            <div class="account-body"> 
                                <div class="table-responsive-sm account-table">
                                    <table class="table" id="psiTable">
                                        <thead>
                                            <tr>
                                                <td>PSI 24 Hourly</td>
                                                <td>PM10 24 Hourly</td>
                                                <td>PM2.5 24 Hourly</td>
                                                <td>CO Sub index</td>
                                                <td>O3 Sub Index</td>
                                                <td>S)2 Sub Index</td>
                                            </tr>
                                        </thead>
                                        <!-- <?php if(isset($regionItems)): ?>
                                            <?php $__currentLoopData = $regionItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e(ucfirst($items->name)); ?></td>
                                                    <td><?php echo e($items->pm10_twenty_four_hourly); ?></td>
                                                    <td><?php echo e($items->pm25_twenty_four_hourly); ?></td>
                                                    <td><?php echo e($items->co_sub_index); ?></td>
                                                    <td><?php echo e($items->co_sub_index); ?></td>
                                                    <td><?php echo e($items->so2_sub_index); ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="6">No Record Found!</td>
                                            </tr>
                                        <?php endif; ?> -->
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 account-right">
                            <!-- Breadcrumbs - START-->
                            <div class="comp comp-account-sidebar" id="ac-1">
                                <div class="comp-holder">
                                    <div class="account-sidebar">
                                        <div class="account-side-nav">
                                            <ul>
                                                <li>
                                                    <a class="bold" href="#">Air Temperature</a>
                                                    <ul class="child-item">
                                                        <li><b>Station Name</b></li>
                                                        <li id="tempName">
                                                            <!-- <?php echo e((isset($stationItems)) ? $stationItems->name : "-"); ?> -->
                                                        </li>
                                                        <li><b>Time Stamp</b></li>
                                                        <li id="tempTime">
                                                            <!-- <?php echo e((isset($stationItems)) ? $stationItems->timestamp : "-"); ?> -->
                                                        </li>
                                                        <li><b>Air Temperature</b></li>
                                                        <li id="tempTemp">
                                                            <!-- <?php echo e((isset($stationItems)) ? $stationItems->value . " Degree" : "-"); ?> -->
                                                        </li>
                                                    </ul>
                                                </i>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Breadcrumbs - END-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\laravel\exam-master\resources\views/home.blade.php ENDPATH**/ ?>