<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('region_id');
            $table->string('o3_sub_index')->nullable();
            $table->string('pm10_twenty_four_hourly')->nullable();
            $table->string('pm10_sub_index')->nullable();
            $table->string('co_sub_index')->nullable();
            $table->string('pm25_twenty_four_hourly')->nullable();
            $table->string('so2_sub_index')->nullable();
            $table->string('co_eight_hour_max')->nullable();
            $table->string('no2_one_hour_max')->nullable();
            $table->string('so2_twenty_four_hourly')->nullable();
            $table->string('pm25_sub_index')->nullable();
            $table->string('psi_twenty_four_hourly')->nullable();
            $table->string('o3_eight_hour_max')->nullable();
            $table->string('timestamp');
            $table->string('update_timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region_items');
    }
}
