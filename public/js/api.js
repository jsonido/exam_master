$(document).ready(function(){
    var _table = '';

    //get data
    $.ajax({
        url         : base_url + "/api/psi",
        type        : "GET",
        processData : false,
        contentType : false,
        success     : function( obj, textStatus, jQxhr ){
            console.log(obj);

            //psi data
            if(obj.psi){

                _table += '<tbody>';

                //iterate through object
                $.each( obj.psi, function( index, items ){
                    _table += '<tr ' + ( (index == 0) ? ' class="alert"' : '' ) + '>'+
                                '<td>' + items.name.charAt(0).toUpperCase() + items.name.slice(1) + '</td>'+
                                '<td>' + items.pm10_twenty_four_hourly + '</td>'+
                                '<td>' + items.pm25_twenty_four_hourly + '</td>'+
                                '<td>' + items.co_sub_index + '</td>'+
                                '<td>' + items.co_sub_index + '</td>'+
                                '<td>' + items.so2_sub_index + '</td>'+
                            '</tr>';
                });

                _table += '</tbody>';
            }
            else {
                _table = '<tr><td colspan="6">No Record Found!</td></tr>';
            }
            $("#psiTable").append(_table);

            //air polution data
            if(obj.air) {
                $("#tempName").html(obj.air.name);
                $("#tempTime").html(obj.air.timestamp);
                $("#tempTemp").html(obj.air.value + " Degree");
            }
            else{
                $("#tempName").html('-');
                $("#tempTime").html('-');
                $("#tempTemp").html('-');
            }
        },
        error : function( jqXhr, textStatus, errorThrown ){
            console.log(errorThrown);
        }
    });
});