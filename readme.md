# README #

This Laravel-based project is a simple webapp that displays information from data.gov, pulling and consolidating
data via API using Laravel framework.

### How to setup? ###

######  Clone repository on local: ######
> git clone git@bitbucket.org:jsonido/exam_master.git

######  IDE/Editor: ######
> Use Visual Studio Code or any Editor of your convenience

######  Database configuration: ######
> Open MySQL and create "exam_master" schema

######  Dependencies: ######
```
1. Open ".env" file and change DB settings
2. Open your terminal and navigate to the project directory on your local
3. Run "php artisan migrate" to auto load/create the needed tables
4. Finaly, run "php aritsan serve"
```

######  How to run tests: ######
** CASE 1: Live API Call **
```
1. make sure you are pulling from "master" branch
2. open your MySQL DB, then select "config"
3. make sure 'sourceOfData' settings has an 'API' value
4. save the db changes
5. call this link "http://127.0.0.1:8000/api/" at the address bar
6. this will fetch the 2 API and save to db
7. then will redirect you to home page (http://127.0.0.1:8000) once done
```

** CASE 2: Json File **
```
1. make sure you are pulling from "change-request" branch
2. open your MySQL DB, then select "config"
3. change the value column of 'sourceOfData' settings to 'FILE'
4. save the db changes
5. call this link "http://127.0.0.1:8000/api/" at the address bar
6. this will fetch the 2 json file store at storage directory and save to db
7. then will redirect you to home page (http://127.0.0.1:8000) once done
```